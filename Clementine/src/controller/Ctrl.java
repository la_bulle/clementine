package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JOptionPane;

import library.SeniorDAO;
import model.Senior;
import view.AjoutSenior;
import view.ChoixSeniorList;
/**
 * Classe CONTROLEUR
 * @author xavier
 *
 */
public class Ctrl implements ActionListener{

	/**
	 * CONSTRUCTEUR priv� de la classe Ctrl
	 */
	private Ctrl(){
	}
	/**
	 * Instance unique pr�-initialis�e
	 */
	private static Ctrl CTRL = new Ctrl();
	/**
	 * Point d'acc�s pour l'instance unique du singleton
	 * @return le singleton Ctrl
	 */
	public static Ctrl getCtrl(){
		return CTRL;
	}

	/**
	 * M�thode d�clanch�e lors de clics sur les boutons de l'application
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		//R�cup�ration de l'action
		String action = evt.getActionCommand();
		//D�coupage et analyse de celle-ci
		String details[] = action.split("_");
		//who : QUI ? Quelle vue a effectu� l'action ?
		String who = details[0];
		//what : QUOI ? Qu'est-ce-que cette vue souhaite faire ?
		String what = details[1];
		//switch-case de traitement des diff�rents cas
		switch(who){
		case "MainView":
			switch(what){
			case "senior":
				//Cr�ation de la vue d'ajout d'un s�nior
				AjoutSenior frame = new AjoutSenior();
				//Assignation d'un observateur sur cette vue
				frame.assignListener(this);
				//Affichage de la vue
				frame.setVisible(true);
				break;
			case "inscription":
				//Cr�ation de la vue de s�lection du s�nior pour lequel on souhaite effectuer des inscriptions
				List<Senior> liste=null;
				try {
					liste = SeniorDAO.getAll();
				} catch (Exception e) {
					String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
					JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
				}
				ChoixSeniorList frame1 = new ChoixSeniorList(liste);
				//Assignation d'un observateur sur cette vue
				frame1.assignListener(this);
				//Affichage de la vue
				frame1.setVisible(true);
				break;
			}
			break;
		case "AjoutSenior":
			switch(what){
			case "valider":
				//R�cup�ration des informations saisies par l'utilisateur
				String nom = AjoutSenior.getTxtName();
				if(nom.equals("")){
					JOptionPane.showMessageDialog(null,"Le nom du s�nior � �t� omis. Veillez � le saisir correctement.","Erreur Saisie",JOptionPane.WARNING_MESSAGE);
					AjoutSenior.focusTxtName();
				}
				else{
					String nomS = AjoutSenior.getTxtName();
					String numS = AjoutSenior.getTxtNumSecu();
					GregorianCalendar dateN = AjoutSenior.getdateChooserNaissance();
					//Cr�ation du nouvel objet Senior
					Senior senior = new Senior(numS,nomS,dateN);
					//INSERT dans la BD
					try {
						SeniorDAO.insert(senior);
						//Message de confirmation pour l'utilisateur
						JOptionPane.showMessageDialog(null,"Le s�nior a bien �t� ajout�","Confirmation Enregistrement",JOptionPane.INFORMATION_MESSAGE);
						//R�initialisation des champs
						AjoutSenior.init();
					} catch (Exception e) {
						String message = "Erreur lors de l'echange avec la base de donn�es. L'application a rencontr�e l'erreur : "+e.getMessage();
						JOptionPane.showMessageDialog(null,message,"Erreur SQL",JOptionPane.ERROR_MESSAGE);
					}
				}
				break;
			case "annuler":
				//R�initialisation des champs
				AjoutSenior.init();
				break;
			}
			break;
		case "InscriptionSenior":
			
			break;
		}	
	}
}
